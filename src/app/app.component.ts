import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'angular-form-array';

  group: FormGroup = null;

  items: Array<string> = ['a', 'b', 'c'];

  constructor(private fb: FormBuilder) {
  }

  ngOnInit(): void {
    this.initForm();
    this.initSubscribe();
  }

  private initSubscribe() {
    for (let inputsKey in this.inputs.controls) {
      this.inputs.controls[ inputsKey ].valueChanges.subscribe(data => console.log(data, inputsKey));
    }
  }

  private initForm() {
    this.group = this.fb.group({
      inputs: this.fb.array([])
    });
    this.items.forEach(x => {
      this.inputs.push(this.addInputArray(x))
    });
  }

  get inputs() {
    return this.group.controls['inputs'] as FormArray;
  }

  /**
   * it also could be a simple FormControl instead of complexe formGroup
   */
  addInputArray(item: string): FormGroup {
    return this.fb.group({
      label: [item],
      value: []
    });
  }


}
